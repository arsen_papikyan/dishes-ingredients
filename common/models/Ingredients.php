<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "ingredients".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $updated_at
 * @property string $created_at
 * @property integer $is_status
 *
 * @property ConnectivityIngredients[] $connectivityIngredients
 */
class Ingredients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'unique'],
            [['is_status'], 'integer'],
            [['title', 'keywords', 'updated_at', 'created_at'], 'string',],
            [['description'], 'string', 'max' => 3000],
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'is_status' => 'Is Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConnectivityIngredients()
    {
        return $this->hasMany(ConnectivityIngredients::className(), ['ingredient_id' => 'id']);
    }
}
