<?php

namespace backend\controllers;

use common\models\Dishes;
use common\models\Ingredients;
use Yii;
use common\models\ConnectivityIngredients;
use backend\models\ConnectivityIngredientsControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ConnectivityIngredientsController implements the CRUD actions for ConnectivityIngredients model.
 */
class ConnectivityIngredientsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ConnectivityIngredients models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConnectivityIngredientsControl();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @param $dishes_id
     * @return bool
     */
    public function actionGetUnselectedIngredient($dishes_id)
    {
        if (!empty($dishes_id)) {
            $ingredientsId = ConnectivityIngredients::find()
                ->select('ingredient_id')
                ->where(['dishes_id' => $dishes_id])
                ->asArray()
                ->all();

            if (!empty($ingredientsId)) {
                $ingredientsId = array_column($ingredientsId, 'ingredient_id');


                $ingredients = Ingredients::find()->select(['id', 'title'])
                    ->andWhere(['not',[ 'id'=> $ingredientsId]])
                    ->asArray()
                    ->all();

                if (!empty($ingredients)) {
                    foreach ($ingredients as $val) {
                        echo "<option value='{$val['id']}'>{$val['title']} </option>";
                    }
                } else {
                    return false;
                }
            } else {
                $ingredients = Ingredients::find()->select(['id', 'title'])
                    ->asArray()
                    ->all();

                if (!empty($ingredients)) {
                    foreach ($ingredients as $val) {
                        echo "<option value='{$val['id']}'>{$val['title']} </option>";
                    }
                }
            }

        }
    }

    /**
     * @param $dishes_id
     * @return bool
     */
    public function actionGetSelectedIngredient($dishes_id)
    {
        if (!empty($dishes_id)) {
            $ingredientsId = ConnectivityIngredients::find()
                ->select('ingredient_id')
                ->where(['dishes_id' => $dishes_id])
                ->asArray()
                ->all();

            if (!empty($ingredientsId)) {
                $ingredientsId = array_column($ingredientsId, 'ingredient_id');

                $ingredients = Ingredients::find()->select(['id', 'title'])
                    ->where(['id' => $ingredientsId])
                    ->asArray()
                    ->all();

                if (!empty($ingredients)) {
                    foreach ($ingredients as $val) {
                        echo "<option value='{$val['id']}'>{$val['title']} </option>";
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Creates a new ConnectivityIngredients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ConnectivityIngredients();

        if ($model->load(Yii::$app->request->post())) {


            $row = [];
            $ingredientsId = json_decode($model->ingredient_id);
            $dishes_id = $model->dishes_id;
            foreach ($ingredientsId as $val) {
                $row[] = [$dishes_id, $val
                ];
            }
            /**
             * delete old data
             */
            ConnectivityIngredients::deleteAll(['dishes_id' => $dishes_id]);

            $connectivityIngredients = Yii::$app->db->createCommand()
                ->batchInsert(
                    'connectivity_ingredients',
                    ['dishes_id', 'ingredient_id'],
                    $row
                )
                ->execute();
            if ($connectivityIngredients) {
                return $this->redirect(['index'], 301);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);

    }



    /**
     * Deletes an existing ConnectivityIngredients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ConnectivityIngredients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ConnectivityIngredients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ConnectivityIngredients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
