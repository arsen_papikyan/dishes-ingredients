<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\redactor\widgets\Redactor;
use kartik\widgets\SwitchInput;
use yii\helpers\Url;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Dishes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dishes-form">

    <?php
    $model_id = $model->id;
    $form = ActiveForm::begin([
        'enableClientValidation' => true,
        'enableAjaxValidation' => false,
        'validateOnChange' => true,
        'validateOnBlur' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]);  ?>
    <div class="col-xs-12">
        <?= $form->field($model, 'is_status')->widget(SwitchInput::classname(), [
            'value' => true,
            'pluginOptions' => [
                'size' => 'large',
                'onColor' => 'success',
                'offColor' => 'danger',
                'onText' => 'Active',
                'offText' => 'Inactive'
            ]
        ]) ?>
    </div>
    <div class="col-xs-12">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'description')->textarea() ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <?= $form->field($model, 'content')->widget(Redactor::className(),
            [
                'clientOptions' =>
                    [
                        'imageUpload' => Url::to(['/redactor/upload/image']),
                        'fileUpload' => false,
                        'plugins' => ['fontcolor', 'imagemanager', 'table', 'undoredo', 'clips', 'fullscreen'],
                    ]
            ]);
        ?>
    </div>
    <div class="col-xs-12">
        <?= $form->field($model, 'short_description')->widget(Redactor::className(),
            [
                'clientOptions' =>
                    [
                        'imageUpload' => Url::to(['/redactor/upload/image']),
                        'fileUpload' => false,
                        'plugins' => ['fontcolor', 'imagemanager', 'table', 'undoredo', 'clips', 'fullscreen'],
                    ]
            ]);
        ?>
    </div>

    <div class="col-xs-12">

        <?php
        $uploadUrl = Url::to(['/dishes/upload-images?id=' . $model_id]);

        $imagesOptions = [];
        $imgPath = [];

        if (!$model->isNewRecord) {
            $imgName = $model->img_name;
            if (!empty($imgName)){
            $imgFullPath =Yii::getAlias("@frontend") . "/web/images/dishes/" . $imgName;

                if (file_exists($imgFullPath)) {
                $deleteUrl = Url::to(["/dishes/delete-file?id=" . $model_id]);

                $imgPath[] = Url::to('/dishes-ingredients/frontend/web/images/dishes/') . $imgName;
                $size = 0;

                    $size = filesize($imgFullPath);


                $imagesOptions[] = [
//                'caption' => $model->title,
                    'url' => $deleteUrl,
                    'size' => $size,
                    'key' => $model_id,

                ];
                }
            }

        }
        ?>
        <?= $form->field($model, 'img_name')->widget(FileInput::classname(), [
            'attribute' => 'img_name',
            'name' => 'img_name',
            'options' => [
                'accept' => 'image/*',
                'multiple' => false,
            ],
            'pluginOptions' => [
                'previewFileType' => 'image',
                "uploadAsync" => true,
                'showPreview' => true,
                'showUpload' => $model->isNewRecord ? false : true,
                'showCaption' => false,
                'showDrag' => false,
                'uploadUrl' => $uploadUrl,
                'initialPreviewConfig' => $imagesOptions,
                'initialPreview' => $imgPath,
                'initialPreviewAsData' => true,
                'initialPreviewShowDelete' => true,
                'overwriteInitial' => true,
                'resizeImages' => true,
                'layoutTemplates' => [
                    !$model->isNewRecord ?: 'actionUpload' => '',
                ],
            ],
        ]);
        ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
