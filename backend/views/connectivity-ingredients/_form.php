<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use common\models\Dishes;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\ConnectivityIngredients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="connectivity-ingredients-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--    --><? //= $form->field($model, 'dishes_id')->textInput() ?>
    <!--    --><? //= $form->field($model, 'ingredient_id')->textInput() ?>
    <div class="col-xs-12">

        <?php

        $dishes = ArrayHelper::map(Dishes::find()
            ->select(['id', 'title'])
            ->where(['is_status' => true])
            ->asArray()
            ->all(),
            'id', 'title');

        echo $form->field($model, 'dishes_id')->widget(Select2::classname(), [
            'data' => $dishes,
//            'disabled' => true,
            'options' => [
                'placeholder' => 'Dishes',
                'onChange' => '
                  
                            $.get("' . Url::toRoute(['/connectivity-ingredients/get-unselected-ingredient']) . '", { 
                            dishes_id: $(this).val() })
                                    .done(function(data){ 
                                        $(".unselected").html( data );
                                         });
                                         
                                         
                                         $.get("' . Url::toRoute(['/connectivity-ingredients/get-selected-ingredient']) . '", { 
                            dishes_id: $(this).val() })
                                    .done(function(data){ 
                                        $(".selected").html( data );
                                         });
                                         
                                         '
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>


    <div class="col-xs-12">
        <div class="form-group">
            <h2> Ingredient </h2>
            <?php echo maksyutin\duallistbox\Widget::widget([
                'model' => $model,
                'attribute' => 'ingredient_id',
                'title' => 'Ingredient',
                'data' => \common\models\Ingredients::find()->where(['is_status' => true]),

                'data_id' => 'id',
                'data_value' => 'title',
                'lngOptions' => [
                    'warning_info' => 'Are you sure you want to select this amount of items?',
                    'search_placeholder' => 'Filter',
                    'showing' => ' - showing',
                    'available' => 'Available',
                    'selected' => 'Selected'
                ]
            ]);
            ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
