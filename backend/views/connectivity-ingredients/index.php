<?php

use yii\helpers\Html;
use  backend\modules\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ConnectivityIngredientsControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Connectivity Ingredients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connectivity-ingredients-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [

                'attribute' => 'dishes_id',
                'value' => function ($model) {
                     return $model['dishes']['title'];
                }
            ],
            [

                'attribute' => 'ingredient_id',
                'value' => function ($model) {
                     return $model['ingredient']['title'];
                }
            ],

            [
                'class' => \yii\grid\ActionColumn::className(),
                'template'=>'{delete}',
            ],
        ],
        'panel' => [
            'before' =>Html::a("<i class='fa fa-pencil' aria-hidden='true'></i> Update Connectivity Ingredients", ['create'], ['class' => 'btn btn-success'])
        ],

    ]); ?>
</div>
