<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ConnectivityIngredients */

$this->title = 'Create Connectivity Ingredients';
$this->params['breadcrumbs'][] = ['label' => 'Connectivity Ingredients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connectivity-ingredients-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
