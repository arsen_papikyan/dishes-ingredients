<?php

use yii\helpers\Html;
use backend\modules\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IngredientsControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ingredients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredients-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn'
            ],
            'id',
            'title',
            'description',
            'keywords',
            'updated_at',
            'created_at',
            'is_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
