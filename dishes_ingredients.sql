/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 5.6.34-log : Database - dishes_ingredients
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `connectivity_ingredients` */

DROP TABLE IF EXISTS `connectivity_ingredients`;

CREATE TABLE `connectivity_ingredients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dishes_id` int(11) DEFAULT NULL,
  `ingredient_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dishes_id` (`dishes_id`),
  KEY `ingredient_id` (`ingredient_id`),
  CONSTRAINT `connectivity_ingredients_ibfk_1` FOREIGN KEY (`dishes_id`) REFERENCES `dishes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `connectivity_ingredients_ibfk_2` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `connectivity_ingredients` */

LOCK TABLES `connectivity_ingredients` WRITE;

insert  into `connectivity_ingredients`(`id`,`dishes_id`,`ingredient_id`) values 
(1,2,3),
(2,2,2),
(3,2,1),
(4,2,7),
(5,2,5),
(6,2,6),
(7,2,4),
(8,1,9),
(9,1,12),
(10,1,11),
(11,1,7),
(12,1,8),
(13,1,13),
(14,1,10);

UNLOCK TABLES;

/*Table structure for table `dishes` */

DROP TABLE IF EXISTS `dishes`;

CREATE TABLE `dishes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL,
  `content` text,
  `short_description` text,
  `img_name` varchar(300) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `keywords` varchar(300) DEFAULT NULL,
  `updated_at` varchar(300) DEFAULT NULL,
  `created_at` varchar(300) DEFAULT NULL,
  `is_status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `dishes` */

LOCK TABLES `dishes` WRITE;

insert  into `dishes`(`id`,`title`,`content`,`short_description`,`img_name`,`description`,`keywords`,`updated_at`,`created_at`,`is_status`) values 
(1,'Салат с помидорами и колбасой','<p>Подготовить колбасу и сыр, помидоры хорошо вымыть, чеснок очистить.<span class=\"redactor-invisible-space\" style=\"background-color: initial;\"></span></p><p><span class=\"redactor-invisible-space\">В миску нарезать соломкой колбасу, сыр натереть на терке.<span class=\"redactor-invisible-space\"><br>Добавить соломкой нарезанные помидоры и прессованный чеснок.<span class=\"redactor-invisible-space\"><br>Заправить майонезом, посолить по вкусу и перемешать.<span class=\"redactor-invisible-space\"><br>Готовый салат выложить на тарелки с зеленью и подавать к столу.</span></span></span></span></p>','<p><em>Этот сочный салат из простых ингредиентов </em></p><p><em>станет вашим любимым рецептом.</em></p>','K_j63zBQqhONBNznmk4m9akHQVHAWXGD.jpg','Салат с помидорами и колбасой','Салат с помидорами и колбасой','2017-11-13 08:29:49',NULL,1),
(2,'Оливье классический','<p>Картофель и морковь отварить, остудить и очистить.<br>Отварить и очистить яйца.<span class=\"redactor-invisible-space\"><br>Нарезать кубиками в миску овощи. Добавить кубиками нарезанную вареную колбасу и яйца.<span class=\"redactor-invisible-space\"><br>Добавить кубиками нарезанные маринованные огурцы и консервированный зеленый горошек.<span class=\"redactor-invisible-space\"><br>Заправить любимым майонезом.<span class=\"redactor-invisible-space\"><br>Перемешать наш оливье и дать настояться в холодильнике.<span class=\"redactor-invisible-space\"><br>Оливье классический готов. Выложить в салатницу и подавать к столу.<span class=\"redactor-invisible-space\"><br></span></span></span></span></span><br></span></p>','<p><em>Салат Оливье классический. </em></p><p><em>Самый любимый и самый популярный салат на наших праздничный</em></p><p><em> и новогодних столах.</em><br></p>','RRMjkK8ZoyFlZcrkElYJvrll4DIHdXE3.jpg','Оливье классический','Оливье классический','2017-11-13 08:38:00','2017-11-13 07:02:24',1);

UNLOCK TABLES;

/*Table structure for table `grid_sort` */

DROP TABLE IF EXISTS `grid_sort`;

CREATE TABLE `grid_sort` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visible_columns` text,
  `default_columns` text,
  `page_size` varchar(300) DEFAULT NULL,
  `class_name` varchar(300) DEFAULT NULL,
  `theme` varchar(300) DEFAULT NULL,
  `label` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `grid_sort` */

LOCK TABLES `grid_sort` WRITE;

insert  into `grid_sort`(`id`,`visible_columns`,`default_columns`,`page_size`,`class_name`,`theme`,`label`,`user_id`) values 
(1,'[\"id\",\"description\",\"keywords\",\"updated_at\"]',NULL,'','common\\models\\Ingredients','','Ingredients',1),
(2,'[\"title\",\"keywords\",\"created_at\",\"is_status\",\"description\",\"updated_at\"]',NULL,'10','common\\models\\Dishes','','Dishes',1),
(3,NULL,NULL,NULL,'common\\models\\ConnectivityIngredients',NULL,'Connectivity Ingredients',1),
(4,NULL,NULL,NULL,'common\\models\\Dishes',NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `ingredients` */

DROP TABLE IF EXISTS `ingredients`;

CREATE TABLE `ingredients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `description` varchar(3000) DEFAULT NULL,
  `keywords` varchar(300) DEFAULT NULL,
  `updated_at` varchar(300) DEFAULT NULL,
  `created_at` varchar(300) DEFAULT NULL,
  `is_status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `ingredients` */

LOCK TABLES `ingredients` WRITE;

insert  into `ingredients`(`id`,`title`,`parent_id`,`description`,`keywords`,`updated_at`,`created_at`,`is_status`) values 
(1,'Консервированный зеленый горошек',0,'Консервированный зеленый горошек','Консервированный зеленый горошек','2017-11-13 07:52:07','2017-11-13 07:52:07',1),
(2,'Колбаса вареная',0,'Колбаса вареная','Колбаса вареная','2017-11-13 07:52:23','2017-11-13 07:52:23',1),
(3,'Картофель',0,'Картофель','Картофель','2017-11-13 07:52:58','2017-11-13 07:52:58',1),
(4,'Яйца',0,'Яйца','Яйца','2017-11-13 07:53:12','2017-11-13 07:53:12',1),
(5,'Морковь',0,'Морковь','Морковь','2017-11-13 07:53:29','2017-11-13 07:53:29',1),
(6,'Огурцы маринованные',0,'Огурцы маринованные','Огурцы маринованные','2017-11-13 07:53:46','2017-11-13 07:53:46',1),
(7,'Майонез',0,'Майонез','Майонез','2017-11-13 07:54:06','2017-11-13 07:54:06',1),
(8,'Помидор',0,'Помидор','Помидор','2017-11-13 07:59:00','2017-11-13 07:59:00',1),
(9,'Вареная колбаса',0,'Вареная колбаса','Вареная колбаса','2017-11-13 07:59:18','2017-11-13 07:59:18',1),
(10,'Сыр',0,'Сыр','Сыр','2017-11-13 07:59:46','2017-11-13 07:59:46',1),
(11,'Зубчик чеснока',0,'Зубчик чеснока','Зубчик чеснока','2017-11-13 08:00:16','2017-11-13 08:00:16',1),
(12,'Зелень (для украшения)',0,'Зелень (для украшения)','Зелень (для украшения)','2017-11-13 08:00:57','2017-11-13 08:00:57',1),
(13,'Соль',0,'Соль','Соль','2017-11-13 08:01:18','2017-11-13 08:01:18',1);

UNLOCK TABLES;

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `migration` */

LOCK TABLES `migration` WRITE;

insert  into `migration`(`version`,`apply_time`) values 
('m000000_000000_base',1509148832),
('m130524_201442_init',1509148835);

UNLOCK TABLES;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

LOCK TABLES `user` WRITE;

insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`) values 
(1,'admin','ydkopt3DoitdlHZtEv5uYIRTyWGktDvS','$2y$13$kf577rmCN6PhAL8nh6RPp.CoQEg7Iy6LAxO4tp.xwBTN09ZLSKlaG',NULL,'admin@mail.ru',10,1509418374,1509418374);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
